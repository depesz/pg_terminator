pg_terminator configuration file
================================

For its work, pg_terminator requires configuration file.

Config file locations
---------------------

Upon starting pg_terminator, it looks for config file in couple of
predefined places:

1. /etc/pg_terminator.yaml
2. /usr/local/etc/pg_terminator.yaml
3. ./pg_terminator.yaml

Then it also checks if there is PG_TERMINATOR_CONFIG environment
variable, and if yes - adds it to list of paths to look for config.

Then, if user provided --config option on command line, its value is
also added to list of potential paths.

When loading, pg_terminator loads *last* config from list.

This means that config provided by user as command line option has the
highest priority, and /etc/pg_terminator.yaml - has the lowest.

This (finding current config file) happens also automatically every so often.

So, if you started pg_terminator with only config in /etc/pg_terminator.conf,
it will load this config, but if you later on added file
/usr/local/etc/pg_terminator.yaml - this one will be reloaded, as it takes
precedence.

Config file format
------------------

Config file has to be proper YAML. It is assumed to be top-level "hash",
with some elements mandatory, and some optional.

Top level elements
------------------

* min_sleep

optional value that will make sure that terminator query will never run in
database more often than X. Value is in seconds. Defaults to 0.

* log

optional (defaults to console) value that sets log destination.
Potential values:

  * console
  * /some/path/to/log/file
  * syslog
  * syslog:FACILITY

  where facility is one of: authpriv, auth, cron, daemon, ftp, kern, lpr, mail,
  news, syslog, user, uucp, or local0 .. local7.

  In case "syslog" is used, it will use facility of daemon.

* database - mandatory hash with settings used to connect to database.
This has 4 potential elements:

  * dbname - mandatory name of database to connect to
  * host - optional name/ip of host to connect to
  * port - optional port number to use when connecting to db
  * user - optional name of user to connect to

Please note that there is no setting for password. If your database
requires password for authentication, please use methods like .pgpass
file.

* rules - mandatory array where each element is a single rule

Rule definition
---------------

Each rule needs 5 parameters, which are:

* name : has to be unique. Is used for reporting.
* action : one of cancel/terminate. Depending on action pg_terminator
           will either call pg_cancel_backend or pg_terminate_backend
           PostgreSQL functions.
* type : one of query/transaction/state/connection
* threshold : time, in seconds, after which connection matching given
              rule will be acted on.
* condition: literal value that will be put in "WHERE" clause to
             pg_stat_activity to find offending queries/backends

It is important to note that value of "condition" will put in literally
in SQL query that will get called in DB server. As such it is of
highest importance to make sure that it is syntactically and logically
correct.

Please note that condition should *not* contain elements that check for
"duration" - these are handled by type/threshold, and are important to
be left for pg_terminator own handling.

Based on type, threshold will relate to various timestamp columns in
pg_stat_activity:

* query : query_start
* transaction : xact_start
* state : state_change
* connection : backend_start

Rule examples:
--------------

Rule that will terminate connections longer than 5 minutes, if these are
not doing anything:

name: too-long-connections
action: terminate
type: connection
threshold: 300
condition: state = 'idle'

Rule that will cancel too long queries (over 1 minute) coming from webapp account:

name: long-webapp-queries
action: cancel
type: query
threshold: 60
condition: usename = 'webapp' and state = 'active'

Rule that will terminate too long transactions (over 3 minutes) coming from webapp account:

name: long-webapp-txns
action: terminate
type: transaction
threshold: 180
condition: usename = 'webapp' and state <> 'idle'
