pg_terminator usage
===================

When you'll first run pg_terminator --help, you will be shown:

```
Usage: pg_terminator [options]
        --config CONFIG_FILE         Full path to config file
        --pidfile PID_FILE           Full path to pid file, default: /var/tmp/pg_terminator.depesz.pidfile
    -v, --verbose                    Enable verbose mode
        --foreground                 Stay in foreground
    -V, --version                    Show version information

Config file is described in configuration.md doc file.
```

Configuration
-------------

One generally needs to create config (you can use one of provided examples),
and put it in one of these locations:

1. /etc/pg_terminator.yaml
2. /usr/local/etc/pg_terminator.yaml
3. ./pg_terminator.yaml

path in 3 means that you put the pg_terminator.yaml file in directory that
you'll run pg_terminator from.

Alternatively you can put the config file anywhere, and set
PG_TERMINATOR_CONFIG environment variable to its path.

As a last way, you can provide path to config using --config option.

Config will be read from the last existing place, in order:

1. /etc
2. /usr/local/etc
3. current dir when running pg_terminator
4. PG_TERMINATOR_CONFIG
5. value of --config option

It is suggested that, in the beginning, you run pg_terminator with
--foreground option, until you'll be sure that it works properly.

Then, you can stop it (ctrl-c), and run it without --foreground, so it will
properly daemonize.

To kill daemonized pg_terminator simply send kill to it. You can find it's pid
in created pidfile.

Config reload
-------------

Whenever pg_terminator finishes it's killing loop it will check if config has
changed, and if yes - reload it.

Please note that if you deleted/created files, it might load config from
another file than it originally loaded it from!

Automatic starting
------------------

Due to sheer number of possible systems, distribution, startup methods, it's
hard to provide a way to start pg_terminator in every combination of them.

As such, I suggest that one simply use cron, and add, in crontab such entry:

```
@reboot /path/to/pg_terminator --config /path/to/config
```

This will make cron start pg_terminator, and it will keep running on its own.

