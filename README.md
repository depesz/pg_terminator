pg_terminator
=============

pg_terminator is a tool for automatically canceling, or terminating
offending connections.

To make it work you have to provide configuration file (check
configuration.md for details), and then run it.

While working pg_terminator will:

* run a query that cancels/terminates backends based on set of rules
* check when it's possible that next offending connection will happen
  (to avoid running terminator query too often)
* reload config if/when necessary

